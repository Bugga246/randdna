#include <string>
#include <random>
#include <vector>
 
using namespace std;
using std::string;

string randDNA(int s, string b, int n)
{

string dna;
std::mt19937 eng1(s);

int min = 1;
int max = b.sizes();

std::uniform_int_distribution<> unifrm(min,max);

for (int i=0; i < b.sizes; i++)
{
	dna += b[unifrm(eng1) - 1];
}

return dna;

}
